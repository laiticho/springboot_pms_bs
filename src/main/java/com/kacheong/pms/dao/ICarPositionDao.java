package com.kacheong.pms.dao;

import com.kacheong.pms.domain.CarPosition;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Date;
import java.util.List;

public interface ICarPositionDao extends IBaseDao<CarPosition> {
    /**
     * 根据车辆ID获取车位与车辆绑定信息
     * @param carId
     * @return
     */
    List getByCarId(Integer carId);

    /**
     * 从记录表中计算停车时长
     * @param carId
     * @param positionId
     * @return
     */
    Integer countSecond(Integer carId, Integer positionId);

    /**
     * 获得所有正在停车的车辆
     * @return
     */
    List<CarPosition> getParkingCar();
}
