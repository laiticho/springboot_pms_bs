package com.kacheong.pms.dao;

import com.kacheong.pms.domain.DeviceType;

public interface IDeviceTypeDao extends IBaseDao<DeviceType> {
    /**
     * 获取设备类型
     * @param name
     * @return
     */
    DeviceType getByName(String name);
}
