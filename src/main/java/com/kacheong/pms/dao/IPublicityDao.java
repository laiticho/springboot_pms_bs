package com.kacheong.pms.dao;

import com.kacheong.pms.domain.Publicity;

public interface IPublicityDao extends IBaseDao<Publicity> {
}
