package com.kacheong.pms.dao;

import com.kacheong.pms.domain.Area;

import java.util.List;

public interface IAreaDao extends IBaseDao<Area> {
    /**
     * 根据停车场ID获取区域信息
     * @param garageId
     * @return
     */
    List<Area> getByGarage(Integer garageId);
}
