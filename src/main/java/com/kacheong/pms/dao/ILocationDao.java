package com.kacheong.pms.dao;

import com.kacheong.pms.domain.Location;

public interface ILocationDao extends IBaseDao<Location> {
    /**
     * 根据位置获取广告位实体
     * @param name
     * @return
     */
    Location getByName(String name);
}
