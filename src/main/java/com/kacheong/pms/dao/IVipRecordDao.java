package com.kacheong.pms.dao;

import com.kacheong.pms.domain.VipRecord;

public interface IVipRecordDao extends IBaseDao<VipRecord> {
}
