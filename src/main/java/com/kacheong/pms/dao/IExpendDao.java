package com.kacheong.pms.dao;

import com.kacheong.pms.domain.Expend;

import java.util.List;

public interface IExpendDao extends IBaseDao<Expend> {
    List getExpendInfo();
}
