package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.IUserDao;
import com.kacheong.pms.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDao extends BaseDao<User> implements IUserDao {
    /**
     * 根据用户的名字查询该用户
     *
     * @param name
     * @return
     */
    @Override
    public User getByName(String name) {
        String hql;
        if (name!=null&&!name.equals("")){
            hql = "FROM User user WHERE user.name = ?";
            List list = getHibernateTemplate().find(hql,name);
            if (list.size()!=0)
                return (User) list.get(0);
        }
        return null;
    }

    /**
     * 根据电话查询用户
     *
     * @param tel
     * @return
     */
    @Override
    public User getByTel(String tel) {
        String hql;
        if (tel!=null&&!tel.equals("")){
            hql = "FROM User user WHERE user.tel = ?";
            List list = getHibernateTemplate().find(hql,tel);
            if (list.size()!=0)
                return (User) list.get(0);
        }
        return null;
    }
}
