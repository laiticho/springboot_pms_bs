package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.IPositionDao;
import com.kacheong.pms.domain.Position;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PositionDao extends BaseDao<Position> implements IPositionDao {
    /**
     * 根据区域ID获取该区域所有停车位
     *
     * @param areaId
     * @return
     */
    @Override
    public List<Position> getByArea(Integer areaId) {
        String hql;
        if (areaId!=null){
            hql = "FROM Position position WHERE position.area.id = ?";
            return (List<Position>) getHibernateTemplate().find(hql,areaId);
        }
        return null;
    }

    /**
     * 根据车位的状态获取停车位
     *
     * @param status
     * @return
     */
    @Override
    public List<Position> getByStatus(String status) {
        String hql;
        if (status!=null&&!status.equals("")){
            hql = "FROM Position position WHERE position.status = ?";
            return (List<Position>) getHibernateTemplate().find(hql,status);
        }
        return null;
    }
}
