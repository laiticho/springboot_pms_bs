package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.ICarDao;
import com.kacheong.pms.domain.Car;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarDao extends BaseDao<Car> implements ICarDao {
    /**
     * 根据车牌号获取车辆实体
     *
     * @param plate
     * @return
     */
    @Override
    public Car getByPlate(String plate) {
        String hql;
        if (plate!=null&&!plate.equals("")){
            hql = "FROM Car car WHERE car.plate = ?";
            List<Car> list = (List<Car>) getHibernateTemplate().find(hql,plate);
            if (list.size()>0)
                return list.get(0);
        }
        return null;
    }

    /**
     * 获取所有年卡月卡车辆
     *
     * @return
     */
    @Override
    public List<Car> getAllVip() {
        String hql="FROM Car car WHERE car.day <> ?";
        List list = getHibernateTemplate().find(hql,0L);
        return list;
    }

    /**
     * 获取所有普通车辆
     *
     * @return
     */
    @Override
    public List<Car> getAllCommon() {
        String hql="FROM Car car WHERE car.day = ?";
        List list = getHibernateTemplate().find(hql,0);
        return list;
    }
}
