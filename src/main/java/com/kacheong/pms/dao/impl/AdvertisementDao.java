package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.IAdvertisementDao;
import com.kacheong.pms.domain.Advertisement;
import org.springframework.stereotype.Service;

@Service
public class AdvertisementDao extends BaseDao<Advertisement> implements IAdvertisementDao {
}
