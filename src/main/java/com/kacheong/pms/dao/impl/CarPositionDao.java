package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.ICarPositionDao;
import com.kacheong.pms.domain.CarPosition;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarPositionDao extends BaseDao<CarPosition> implements ICarPositionDao {

    /**
     * 根据车辆ID获取车位与车辆绑定信息
     * @param carId
     * @return
     */
    @Override
    public List getByCarId(Integer carId) {
        String hql;
        if (carId!=null){
            hql = "FROM CarPosition carPosition WHERE carPosition.car.id = ?";
            List list = getHibernateTemplate().find(hql,carId);
            return list;
        }
        return null;
    }

    /**
     * 从记录表中计算停车时长
     *
     * @param carId
     * @param positionId
     * @return
     */
    @Override
    public Integer countSecond(Integer carId,Integer positionId) {
        String hql;
        if (carId!=null&&positionId!=null){
            hql = "SELECT ROUND(TIMESTAMPDIFF(SECOND,carPosition.inTime,carPosition.outTime)/1,2) FROM CarPosition carPosition WHERE carPosition.car.id = ? AND carPosition.position.id = ?";
            List list = getHibernateTemplate().find(hql,carId,positionId);
            if (list.size()>0){
                return (Integer) list.get(0);
            }
        }
        return null;
    }

    /**
     * 获得所有正在停车的车辆
     *
     * @return
     */
    @Override
    public List<CarPosition> getParkingCar() {
        String hql = "FROM CarPosition cp WHERE cp.outTime = ?";
        return (List<CarPosition>) getHibernateTemplate().find(hql,null);
    }
}
