package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.ILocationDao;
import com.kacheong.pms.domain.Location;
import org.springframework.stereotype.Repository;

import javax.xml.ws.Action;
import java.util.List;

@Repository
public class LocationDao extends BaseDao<Location> implements ILocationDao {

    /**
     * 根据位置获取广告位实体
     *
     * @param name
     * @return
     */
    @Override
    public Location getByName(String name) {
        String hql = "FROM Location location WHERE location.name = ?";
        List list = getHibernateTemplate().find(hql,name);
        if (list.size()>0)
            return (Location) list.get(0);
        else
            return null;
    }
}
