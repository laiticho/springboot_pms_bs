package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.IPublicityDao;
import com.kacheong.pms.domain.Publicity;
import org.springframework.stereotype.Repository;

@Repository
public class PublicityDao extends BaseDao<Publicity> implements IPublicityDao {
}
