package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.IAreaDao;
import com.kacheong.pms.domain.Area;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AreaDao extends BaseDao<Area> implements IAreaDao {


    /**
     * 根据停车场ID获取区域信息
     *
     * @param garageId
     * @return
     */
    @Override
    public List<Area> getByGarage(Integer garageId) {
        String hql;
        if (garageId!=null){
            hql = "FROM Area area WHERE area.garage.id = ?";
            return (List<Area>) getHibernateTemplate().find(hql,garageId);
        }
        return null;
    }
}
