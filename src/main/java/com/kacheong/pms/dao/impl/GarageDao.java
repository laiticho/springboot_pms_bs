package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.IGarageDao;
import com.kacheong.pms.domain.Garage;
import org.springframework.stereotype.Repository;

@Repository
public class GarageDao extends BaseDao<Garage> implements IGarageDao {
}
