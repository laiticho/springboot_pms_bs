package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.IMaintainTypeDao;
import com.kacheong.pms.domain.MaintainType;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MaintainTypeDao extends BaseDao<MaintainType> implements IMaintainTypeDao {
    /**
     * 根据type获取实体
     *
     * @param type
     * @return
     */
    @Override
    public MaintainType getBytype(String type) {
        String hql="FROM MaintainType maintainType WHERE maintainType.type =?";
        List list = getHibernateTemplate().find(hql,type);
        if (list.size()>0)
            return (MaintainType) list.get(0);
        return null;
    }
}
