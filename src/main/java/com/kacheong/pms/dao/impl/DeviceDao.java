package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.IDeviceDao;
import com.kacheong.pms.domain.Device;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeviceDao extends BaseDao<Device> implements IDeviceDao {
    /**
     * 获取设备
     *
     * @param name
     * @return
     */
    @Override
    public Device getByName(String name) {
        String hql = "FROM Device device WHERE device.info = ?";
        List list = getHibernateTemplate().find(hql,name);
        if (list.size()>0)
            return (Device) list.get(0);
        return null;
    }
}
