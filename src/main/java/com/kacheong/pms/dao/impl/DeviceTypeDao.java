package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.IDeviceTypeDao;
import com.kacheong.pms.domain.DeviceType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeviceTypeDao extends BaseDao<DeviceType> implements IDeviceTypeDao {
    /**
     * 获取设备类型
     *
     * @param name
     * @return
     */
    @Override
    public DeviceType getByName(String name) {
        String hql = "FROM DeviceType deviceType WHERE deviceType.type = ?";
        List list = getHibernateTemplate().find(hql,name);
        if (list.size()>0)
            return (DeviceType) list.get(0);
        return null;
    }
}
