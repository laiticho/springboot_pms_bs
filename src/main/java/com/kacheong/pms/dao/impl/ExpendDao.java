package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.IExpendDao;
import com.kacheong.pms.domain.Expend;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExpendDao extends BaseDao<Expend> implements IExpendDao {
    @Override
    public List getExpendInfo() {
        String sql = "SELECT e.expendType.type,sum(e.expend) FROM Expend e GROUP BY e.expendType.id";
        return getHibernateTemplate().find(sql);
    }
}
