package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.IMaintainDao;
import com.kacheong.pms.domain.Maintain;
import org.springframework.stereotype.Repository;

@Repository
public class MaintainDao extends BaseDao<Maintain> implements IMaintainDao {
}
