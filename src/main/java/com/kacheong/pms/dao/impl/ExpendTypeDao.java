package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.IExpendTypeDao;
import com.kacheong.pms.domain.ExpendType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExpendTypeDao extends BaseDao<ExpendType> implements IExpendTypeDao {
    /**
     * 获取支出类型实体
     *
     * @param name
     * @return null/ExpendType
     */
    @Override
    public ExpendType getByName(String name) {
        String hql ="FROM ExpendType expendType WHERE expendType.type =?";
        List list = getHibernateTemplate().find(hql,name);
        if (list.size()>0){
            return (ExpendType) list.get(0);
        }
        return null;
    }
}
