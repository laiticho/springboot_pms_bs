package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.IVipRecordDao;
import com.kacheong.pms.domain.VipRecord;
import org.springframework.stereotype.Repository;

@Repository
public class VipRecordDao extends BaseDao<VipRecord> implements IVipRecordDao {
}
