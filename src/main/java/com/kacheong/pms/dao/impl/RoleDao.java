package com.kacheong.pms.dao.impl;

import com.kacheong.pms.dao.IRoleDao;
import com.kacheong.pms.domain.Role;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by kacheong on 2017/11/17.
 */
@Repository
public class RoleDao extends BaseDao<Role> implements IRoleDao {

    @Override
    public Role getByRole(String role) {
        String hql;
        if (!role.equals("")&&role!=null){
            hql = "FROM Role role WHERE role.role = ?";
            List list = getHibernateTemplate().find(hql,role);
            return (Role) list.get(0);
        }
        return null;
    }
}
