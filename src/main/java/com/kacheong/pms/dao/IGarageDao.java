package com.kacheong.pms.dao;

import com.kacheong.pms.domain.Garage;

public interface IGarageDao extends IBaseDao<Garage> {
}
