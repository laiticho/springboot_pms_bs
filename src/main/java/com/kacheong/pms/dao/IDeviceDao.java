package com.kacheong.pms.dao;

import com.kacheong.pms.domain.Device;

public interface IDeviceDao extends IBaseDao<Device> {
    /**
     * 获取设备
     * @param name
     * @return
     */
    Device getByName(String name);
}
