package com.kacheong.pms.dao;

import com.kacheong.pms.domain.MaintainType;

public interface IMaintainTypeDao extends IBaseDao<MaintainType> {
    /**
     * 根据type获取实体
     * @param type
     * @return
     */
    MaintainType getBytype(String type);
}
