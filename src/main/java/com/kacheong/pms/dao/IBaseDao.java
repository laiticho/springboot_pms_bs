package com.kacheong.pms.dao;

import com.kacheong.pms.POJO.PageBean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kacheong on 2017/11/17.
 */
public interface IBaseDao<T> {
    T get(Serializable id);

    /**
     * 获取全部数据
     *
     * @return
     */
    List<T> list();

    /**
     * 储存数据
     *
     * @param entity 数据
     * @return 结果
     */
    Serializable save(T entity);

    /**
     * 更新数据
     *
     * @param entity 数据
     */
    boolean update(T entity);

    /**
     * 更新数据
     *
     * @param HQL
     * @param param
     * @return 修改记录数
     */
    int update(String HQL, Object... param);

    /**
     * 删除数据
     *
     * @param entity 数据
     */
    void remove(T entity);

    /**
     * 分页操作
     *
     * @param HQL
     * @param currentPage
     * @param pageSize
     * @param param
     * @return
     */
    PageBean doPage(String HQL, int currentPage, int pageSize, Object... param);

    /**
     * 求COUNT
     *
     * @param HQL hql语句
     */
    Long count(String HQL, Object... param  );


}
