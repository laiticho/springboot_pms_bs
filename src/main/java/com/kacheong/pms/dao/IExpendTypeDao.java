package com.kacheong.pms.dao;

import com.kacheong.pms.domain.ExpendType;

public interface IExpendTypeDao extends IBaseDao<ExpendType> {

    /**
     * 获取支出类型实体
     * @param name
     * @return
     */
    ExpendType getByName(String name);
}
