package com.kacheong.pms.dao;

import com.kacheong.pms.domain.Position;

import java.util.List;

public interface IPositionDao extends IBaseDao<Position> {
    /**
     * 根据区域ID获取该区域所有停车位
     * @param areaId
     * @return
     */
    List<Position> getByArea(Integer areaId);

    /**
     * 根据车位的状态获取停车位
     * @param status
     * @return
     */
    List<Position> getByStatus(String status);
}
