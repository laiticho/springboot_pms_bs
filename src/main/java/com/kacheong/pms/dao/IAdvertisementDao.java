package com.kacheong.pms.dao;

import com.kacheong.pms.domain.Advertisement;

public interface IAdvertisementDao extends IBaseDao<Advertisement> {
}
