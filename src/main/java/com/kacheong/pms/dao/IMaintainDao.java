package com.kacheong.pms.dao;

import com.kacheong.pms.domain.Maintain;

public interface IMaintainDao extends IBaseDao<Maintain> {
}
