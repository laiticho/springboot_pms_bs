package com.kacheong.pms.dao;

import com.kacheong.pms.domain.Car;

import java.util.List;

public interface ICarDao extends IBaseDao<Car> {
    /**
     * 根据车牌号获取车辆实体
     * @param plate
     * @return
     */
    Car getByPlate(String plate);

    /**
     * 获取所有年卡月卡车辆
     * @return
     */
    List<Car> getAllVip();

    /**
     * 获取所有普通车辆
     * @return
     */
    List<Car> getAllCommon();
}
