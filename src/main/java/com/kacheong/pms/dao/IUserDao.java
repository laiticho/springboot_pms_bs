package com.kacheong.pms.dao;

import com.kacheong.pms.domain.User;

/**
 * Created by kacheong on 2017/11/17.
 */
public interface IUserDao extends IBaseDao<User>{
    /**
     * 根据用户的名字查询该用户
     * @param name
     * @return
     */
    User getByName(String name);

    /**
     * 根据电话查询用户
     * @param tel
     * @return
     */
    User getByTel(String tel);
}
