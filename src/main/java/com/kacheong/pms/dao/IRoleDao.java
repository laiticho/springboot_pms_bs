package com.kacheong.pms.dao;

import com.kacheong.pms.domain.Role;

/**
 * Created by kacheong on 2017/11/17.
 */
public interface IRoleDao extends IBaseDao<Role>{
    /**
     * 根据角色名查询该角色实体
     * @param role
     * @return
     */
    Role getByRole(String role);
}
