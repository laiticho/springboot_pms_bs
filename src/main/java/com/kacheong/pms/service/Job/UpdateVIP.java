package com.kacheong.pms.service.Job;


import com.kacheong.pms.dao.ICarDao;
import com.kacheong.pms.domain.Car;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import java.util.Iterator;
import java.util.List;

@Configuration
@EnableScheduling
public class UpdateVIP {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private ICarDao carDao;

    @Scheduled(cron = "0 0 0 * * ?")
//    @Scheduled(fixedRate = 60000)
    public synchronized void updateVip(){
        List carList = carDao.getAllVip();
        Iterator it = carList.iterator();
        while (it.hasNext()){
            Car car = (Car) it.next();
            Long day = car.getDay();
            logger.info("车辆ID:"+car.getId()+" 原天数为:"+car.getDay());
            car.setDay(day-1);
            carDao.update(car);
        }
    }
}
