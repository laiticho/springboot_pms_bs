package com.kacheong.pms.service;

public interface IGarageConfigService {
    /**
     * 好像这个东西没什么用
     * @param garageId
     */
    void clearGarage(Integer garageId);

    /**
     * 根据停车场ID去初始化区域(默认为3个区域)
     * @param garageId
     */
    void initDefaultArea(Integer garageId);

    /**
     * 上两个方法
     * @param garageId
     */
    void initDefaultGarage(Integer garageId);

    /**
     * 初始化某个区域的停车位
     * @param areaId
     */
    void initPosition(Integer areaId);

    /**
     * 根据位置信息删除广告位
     * @param name
     * @return
     */
    String delLocation(String name);

    /**
     * 根据类型名删除设备类型
     * @param type
     * @return
     */
    String delDeviceType(String type);

    /**
     * 根据类型名删除支出类型
     * @param type
     * @return
     */
    String delExpendType(String type);

    /**
     * 修改设备信息
     * @param type
     * @param name
     * @param location
     * @param status
     * @return
     */
    String editDevice(Integer id,String type,String name,String location,String status);

    /**
     * 修改设备类型
     * @param id
     * @param type
     * @return
     */
    String editDeviceType(Integer id,String type);

    /**
     * 修改支出类别
     * @param id
     * @param type
     * @return
     */
    String editExpendType(Integer id,String type);
}
