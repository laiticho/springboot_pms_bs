package com.kacheong.pms.service;

import com.kacheong.pms.domain.Role;

public interface IPersonService {
    /**
     * 根据用户获取角色
     * @param userId
     * @return
     */
    Role getRoleByUser(Integer userId);

    /**
     * 修改用户所属角色
     * @param userId
     * @param roleId
     */
    String editUserRole(Integer userId, Integer roleId);

    /**
     * 登陆
     * @param name
     * @param password
     * @return
     */
    String login(String name,String password);
}
