package com.kacheong.pms.service.Impl;

import com.kacheong.pms.Value.StaticValue;
import com.kacheong.pms.dao.IRoleDao;
import com.kacheong.pms.dao.IUserDao;
import com.kacheong.pms.domain.Role;
import com.kacheong.pms.domain.User;
import com.kacheong.pms.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.soap.SOAPBinding;

@Service
public class PersonService implements IPersonService {
    @Autowired
    private IUserDao userDao;

    @Autowired
    private IRoleDao roleDao;
    /**
     * 根据用户获取角色
     *
     * @param userId
     * @return
     */
    @Override
    public Role getRoleByUser(Integer userId) {
        User user = userDao.get(userId);
        return user.getRole();
    }

    /**
     * 修改用户所属角色
     *
     * @param userId
     * @param roleId
     */
    @Override
    public String editUserRole(Integer userId, Integer roleId) {
        //根据用户ID获取用户
        User user = new User();
        if (userId!=null&&userId!=0){
            user = userDao.get(userId);
            if (roleDao.get(roleId)!=null){
                user.setRole(roleDao.get(roleId));
                userDao.update(user);
                return StaticValue.success;
            }
        }
        return StaticValue.error;
    }

    /**
     * 登陆
     *
     * @param name
     * @param password
     * @return
     */
    @Override
    public String login(String name, String password) {
        if (userDao.getByName(name)!=null){
            User user = userDao.getByName(name);
            if (user.getPassword().equals(password)){
                    return "success";
            }
        }
        return null;
    }
}
