package com.kacheong.pms.service.Impl;

import com.kacheong.pms.Value.StaticValue;
import com.kacheong.pms.dao.*;
import com.kacheong.pms.domain.*;
import com.kacheong.pms.service.IGarageConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GarageConfigService implements IGarageConfigService{
    @Autowired
    private ILocationDao locationDao;

    @Autowired
    private IGarageDao garageDao;

    @Autowired
    private IAreaDao areaDao;

    @Autowired
    private IPositionDao positionDao;

    @Autowired
    private IDeviceTypeDao deviceTypeDao;

    @Autowired
    private IExpendTypeDao expendTypeDao;

    @Autowired
    private IDeviceDao deviceDao;

    @Autowired
    private IExpendDao expendDao;

    /**
     * 默认清空车库
     * @return
     */
    public void clearGarage(Integer garageId){
        if (garageDao.get(garageId)!=null){
            long a = new Date().getTime();
            System.out.print(a);
            Garage garage = garageDao.get(garageId);
            garage.setPublicitySet(null);
            garage.setAreaSet(null);
            garage.setInfo("这里是XXX停车场");
            garage.setName("停车场");
            garageDao.update(garage);
        }
    }

    /**
     * 默认初始化区域
     * @return
     */
    public void initDefaultArea(Integer garageId){
        if (garageDao.get(garageId)!=null){
            Garage garage = garageDao.get(garageId);
            List list = areaDao.getByGarage(garageId);
            Iterator it = list.iterator();
            while (it.hasNext()){
                Area area = (Area) it.next();
                areaDao.remove(area);
            }
                for (int i=0;i<3;i++){
                    Area area = new Area();
                    area.setGarage(garage);
                    if (i==0)
                        area.setInfo("Area "+"A");
                    else if (i==1)
                        area.setInfo("Area "+"B");
                    else if (i==2)
                        area.setInfo("Area "+"C");
                    areaDao.save(area);
                }
        }
    }

    /**
     * 默认初始化车库
     * @param garageId
     * @return
     */
    public void initDefaultGarage(Integer garageId){
        //调用方法清空车库
        if (garageId!=null){
            clearGarage(garageId);
            initDefaultArea(garageId);
        }
    }

    @Override
    public void initPosition(Integer areaId) {
        if (areaId!=null){
            Area area = areaDao.get(areaId);
            if(area!=null){
                List<Position> list = positionDao.getByArea(areaId);
//                清空旧的停车位
                Iterator it = list.iterator();
                while (it.hasNext()){
                    Position position = (Position) it.next();
                    positionDao.remove(position);
                }
//                增加新的停车位
                for (int i =0;i<50;i++){
                    Position position = new Position();
                    position.setArea(area);
                    position.setName(Integer.toString(i+1)+"号位");
                    position.setStatus("Empty");
                    positionDao.save(position);
                }
            }
        }
    }

    /**
     * 根据位置信息删除广告位
     *
     * @param name
     * @return
     */
    @Override
    public String delLocation(String name) {
        if (locationDao.getByName(name)!=null){
            Location location = locationDao.getByName(name);
            locationDao.remove(location);
            return StaticValue.success;
        }
        return null;
    }

    /**
     * 根据类型名删除设备类型
     *
     * @param type
     * @return
     */
    @Override
    public String delDeviceType(String type) {
        if (type!=null&&!type.equals("")){
            if (deviceTypeDao.getByName(type)!=null){
                deviceTypeDao.remove(deviceTypeDao.getByName(type));
                return StaticValue.success;
            }
        }
        return null;
    }

    /**
     * 根据类型名删除支出类型
     *
     * @param type
     * @return
     */
    @Override
    public String delExpendType(String type) {
        if (type!=null&&!type.equals("")){
            if (expendTypeDao.getByName(type)!=null){
                if (expendTypeDao.getByName(type).getExpendSet().isEmpty()) {
                    expendTypeDao.remove(expendTypeDao.getByName(type));
                    return StaticValue.success;
                }
            }
        }
        return null;
    }

    /**
     * 修改设备信息
     *
     * @param type
     * @param name
     * @param location
     * @param status
     * @return
     */
    @Override
    public String editDevice(Integer id,String type, String name, String location, String status) {
        Device device = deviceDao.get(id);
        if (type!=null&&name!=null&&location!=null&&status!=null&&!type.equals("")&&!name.equals("")
                &&!location.equals("")&&!status.equals("")){
            device.setStatus(status);
            device.setLocation(location);
            device.setInfo(name);
            device.setDeviceType(deviceTypeDao.getByName(type));
            deviceDao.update(device);
            return StaticValue.success;
        }
        return null;
    }

    /**
     * 修改设备类型
     *
     * @param id
     * @param type
     * @return
     */
    @Override
    public String editDeviceType(Integer id, String type) {
        DeviceType deviceType = deviceTypeDao.get(id);
        if (type!=null&&!type.equals("")) {
            deviceType.setType(type);
            deviceTypeDao.update(deviceType);
            return StaticValue.success;
        }
        return null;
    }

    /**
     * 修改支出类别
     *
     * @param id
     * @param type
     * @return
     */
    @Override
    public String editExpendType(Integer id, String type) {
        ExpendType expendType = expendTypeDao.get(id);
        if (type!=null&&!type.equals("")) {
            expendType.setType(type);
            expendTypeDao.update(expendType);
            return StaticValue.success;
        }
        return null;
    }

}
