package com.kacheong.pms.service.Impl;

import com.kacheong.pms.Value.StaticValue;
import com.kacheong.pms.dao.IMaintainTypeDao;
import com.kacheong.pms.domain.MaintainType;
import com.kacheong.pms.service.IServeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class ServeService implements IServeService {
    @Autowired
    private IMaintainTypeDao maintainTypeDao;
    /**
     * 根据type名字删除保养服务类型
     *
     * @param type
     * @return
     */
    @Override
    public String delMaintainType(String type) {
        if (type!=null&&!type.equals("")){
            if (maintainTypeDao.getBytype(type)!=null){
                maintainTypeDao.remove(maintainTypeDao.getBytype(type));
                return StaticValue.success;
            }
        }
        return null;
    }

    /**
     * 修改保养服务类型
     *
     * @param id
     * @param type
     * @return
     */
    @Override
    public String editMaintainType(Integer id, String type) {
        MaintainType maintainType = maintainTypeDao.get(id);
        if (type!=null&&!type.equals("")){
            maintainType.setType(type);
            maintainTypeDao.update(maintainType);
            return StaticValue.success;
        }
        return null;
    }
}
