package com.kacheong.pms.service.Impl;

import com.kacheong.pms.Util.NumberUtil;
import com.kacheong.pms.Value.StaticValue;
import com.kacheong.pms.dao.ICarDao;
import com.kacheong.pms.dao.ICarPositionDao;
import com.kacheong.pms.dao.IPositionDao;
import com.kacheong.pms.dao.IVipRecordDao;
import com.kacheong.pms.domain.Car;
import com.kacheong.pms.domain.CarPosition;
import com.kacheong.pms.domain.Position;
import com.kacheong.pms.domain.VipRecord;
import com.kacheong.pms.service.IParkingService;
import easypr.core.CharsRecognise;
import easypr.core.PlateDetect;
import org.bytedeco.javacpp.opencv_core;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static org.bytedeco.javacpp.opencv_highgui.imread;

@Service
public class ParkingService implements IParkingService {

    @Autowired
    private ICarDao carDao;

    @Autowired
    private IVipRecordDao vipRecordDao;

    @Autowired
    private ICarPositionDao carPositionDao;

    @Autowired
    private IPositionDao positionDao;
    /**
     * 车辆进入停车场
     *
     * @return
     */
    @Override
    public Map<String, Object> parkingCar(String plate) {
        Map<String,Object> results = new HashMap<>();
        if (plate.equals(""))
             plate = StaticValue.AutoPlate();
        results.put("plate",plate);
        //该车辆以前停过车
        Car car = judgeCar(plate);
        //            车辆与车位绑定
        CarPosition carPosition = new CarPosition();
        carPosition.setCar(car);
        carPosition.setInTime(new Date());
        //随机选取一个空车位进行停车
        List list = positionDao.getByStatus("Empty");
        if (list.size()>0){
            Random random = new Random();
            int j = random.nextInt(list.size());
            Position position = (Position) list.get(j);
            carPosition.setPosition(position);
            results.put("area",position.getArea().getInfo());
            results.put("position",position.getName());
            try {
                carPositionDao.save(carPosition);
                //将车位的status修改为Used
                position.setStatus("Used");
                positionDao.update(position);
                return results;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    /**
     * 车辆离开停车场
     *
     * @param plate
     * @return
     */
    @Override
    public String leavingCar(String plate) {
//        根据车牌获取车辆实体
        if (plate!=null&&!plate.equals("")){
            if (carDao.getByPlate(plate)!=null){
                Car car = carDao.getByPlate(plate);
                List list = carPositionDao.getByCarId(car.getId());
                CarPosition carPosition = (CarPosition) list.get(list.size()-1);
//                outTime为空
                if (carPosition.getOutTime()==null){
                    Position position = positionDao.get(carPosition.getPosition().getId());
                    position.setStatus("Empty");
                    positionDao.update(position);
                    carPosition.setOutTime(new Date());
                    carPositionDao.update(carPosition);
                    Integer second = carPositionDao.countSecond(car.getId(),position.getId());
                    NumberUtil numberUtil = new NumberUtil();
                    Integer hour = numberUtil.secondToHour(second);
                    carPosition.setCost(calculateCost(car.getPlate(),StaticValue.discount,hour));
                    carPositionDao.update(carPosition);
                    return StaticValue.success;
                }
            }
        }
        return StaticValue.error;
    }

    /**
     * 注册年卡月卡
     *
     * @param plate
     * @param day
     * @return
     */
    @Override
    public String registerVip(String plate, Long day,Integer month) {
        Car car;
        if (plate!=null&&!plate.equals(""))
            if (carDao.getByPlate(plate)!=null){
                car = carDao.getByPlate(plate);
                car.setDay(day);
                carDao.update(car);
                vipCarRec(car,month);
                return StaticValue.success;
            }
            else {
                car = new Car();
                car.setPlate(plate);
                car.setDay(day);
                carDao.save(car);
                vipCarRec(car,month);
                return StaticValue.success;
            }
        return null;
    }

    /**
     * 车辆离开停车场时计费
     *
     * @param plate
     * @return
     */
    @Override
    public Double calculateCost(String plate,Double discount,Integer hour) {
        if (plate!=null){
            Car car = carDao.getByPlate(plate);
            if (car.getDay()==0L){
                discount = 1.00;
            }
            return discount*10*hour;
        }
        return null;
    }

    @Override
    public Map<String, Object> manParking(String plate,Integer pId,Integer aId) {
        Map<String,Object> results = new HashMap<>();
        results.put("plate",plate);
        //该车辆以前停过车
        Car car = judgeCar(plate);
        //            车辆与车位绑定
        CarPosition carPosition = new CarPosition();
        carPosition.setCar(car);
        carPosition.setInTime(new Date());
        carPosition.setPosition(positionDao.get(pId));
        carPositionDao.save(carPosition);
        results.put("area",carPosition.getPosition().getArea().getInfo());
        results.put("position",carPosition.getPosition().getName());
        return results;
    }

    private void vipCarRec(Car car,Integer month){
        VipRecord vipRecord = new VipRecord();
        vipRecord.setCar(car);
        vipRecord.setCreateTime(new Date());
        vipRecord.setMonth(month);
        if (month==1)
            vipRecord.setCost(10.0);
        else if (month==3)
            vipRecord.setCost(28.0);
        else if (month==6)
            vipRecord.setCost(55.0);
        else if (month==12)
            vipRecord.setCost(110.0);
        vipRecordDao.save(vipRecord);
    }

    private Car judgeCar(String plate){
        Car car;
        if (carDao.getByPlate(plate)!=null){
            car = carDao.getByPlate(plate);
        }else {
            car = new Car();
            car.setPlate(plate);
            car.setDay(0L);
            carDao.save(car);
        }
        return car;
    }
}
