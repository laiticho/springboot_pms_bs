package com.kacheong.pms.service;

public interface IServeService {
    /**
     * 根据type名字删除保养服务类型
     * @param type
     * @return
     */
    String delMaintainType(String type);

    /**
     * 修改保养服务类型
     * @param id
     * @param type
     * @return
     */
    String editMaintainType(Integer id,String type);
}
