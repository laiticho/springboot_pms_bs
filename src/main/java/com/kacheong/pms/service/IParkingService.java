package com.kacheong.pms.service;

import java.util.Map;

public interface IParkingService {
    /**
     * 车辆进入停车场
     * @return
     */
    Map<String,Object> parkingCar(String plate);

    /**
     * 某车辆离开停车场
     * @param plate
     * @return
     */
    String leavingCar(String plate);

    /**
     * 注册年卡月卡
     * @param plate
     * @param day
     * @return
     */
    String registerVip(String plate,Long day,Integer month);

    /**
     * 车辆离开停车场时计费
     * @param plate
     * @return
     */
    Double calculateCost(String plate, Double discount,Integer hour);

    Map<String,Object> manParking(String plate,Integer pId,Integer aId);
}
