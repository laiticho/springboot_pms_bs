package com.kacheong.pms.domain;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by kacheong on 2017/11/16.
 */
@Entity
@Table(name = "expendType")
public class ExpendType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * 支出类型 e.g. 维修
     */
    @Column(name = "type")
    private String type;

    @OneToMany(mappedBy = "expendType",cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    private Set<Expend> expendSet;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<Expend> getExpendSet() {
        return expendSet;
    }

    public void setExpendSet(Set<Expend> expendSet) {
        this.expendSet = expendSet;
    }

}
