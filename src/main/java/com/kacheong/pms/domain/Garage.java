package com.kacheong.pms.domain;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by kacheong on 2017/11/14.
 */
@Entity
@Table(name = "garage")
public class Garage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * 车库名字
     */
    @Column(name = "name")
    private String name;

    /**
     * 车库简介
     */
    @Column(name = "info")
    private String info;

    @OneToMany(mappedBy = "garage",cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    private Set<Area> areaSet;

    @OneToMany(mappedBy = "garage",cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    private Set<Publicity> publicitySet;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Set<Area> getAreaSet() {
        return areaSet;
    }

    public void setAreaSet(Set<Area> areaSet) {
        this.areaSet = areaSet;
    }

    public Set<Publicity> getPublicitySet() {
        return publicitySet;
    }

    public void setPublicitySet(Set<Publicity> publicitySet) {
        this.publicitySet = publicitySet;
    }

}
