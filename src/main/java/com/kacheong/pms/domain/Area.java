package com.kacheong.pms.domain;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by kacheong on 2017/11/14.
 */
@Entity
@Table(name = "area")
public class Area {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * 车库区域的具体信息 e.g. A区
     */
    @Column(name = "info")
    private String info;

    @OneToMany(mappedBy = "area",cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    private Set<Position> positionSet;

    /**
     * 区域所属车库的ID(暂设置只有一个车库)
     */
    @ManyToOne( fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "garageId")
    private Garage garage;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Set<Position> getPositionSet() {
        return positionSet;
    }

    public void setPositionSet(Set<Position> positionSet) {
        this.positionSet = positionSet;
    }

    public Garage getGarage() {
        return garage;
    }

    public void setGarage(Garage garage) {
        this.garage = garage;
    }
}
