package com.kacheong.pms.domain;

import javax.persistence.*;

/**
 * Created by kacheong on 2017/11/16.
 */
@Entity
@Table(name = "expend")
public class Expend {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * 支出类型ID
     */
    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.PERSIST},fetch = FetchType.EAGER)
    @JoinColumn(name = "expendTypeId")
    private ExpendType expendType;

    /**
     * 支出详细信息描述
     */
    @Column(name = "info")
    private String info;

    /**
     * 支出的金额
     */
    @Column(name = "expend")
    private Double expend;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ExpendType getExpendType() {
        return expendType;
    }

    public void setExpendType(ExpendType expendType) {
        this.expendType = expendType;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Double getExpend() {
        return expend;
    }

    public void setExpend(Double expend) {
        this.expend = expend;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Expend expend1 = (Expend) o;

        if (id != expend1.id) return false;
        if (expendType != null ? !expendType.equals(expend1.expendType) : expend1.expendType != null) return false;
        if (info != null ? !info.equals(expend1.info) : expend1.info != null) return false;
        return expend != null ? expend.equals(expend1.expend) : expend1.expend == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (expendType != null ? expendType.hashCode() : 0);
        result = 31 * result + (info != null ? info.hashCode() : 0);
        result = 31 * result + (expend != null ? expend.hashCode() : 0);
        return result;
    }
}
