package com.kacheong.pms.domain;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by kacheong on 2017/11/16.
 */
@Entity
@Table(name = "maintain")
public class Maintain {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * 保养详细信息
     */
    @Column(name = "info")
    private String  info;

    /**
     * 保养类型ID
     */
    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.PERSIST},fetch = FetchType.EAGER)
    @JoinColumn(name = "maintainTypeId")
    private MaintainType maintainType;

    /**
     * 客户ID
     */
    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.PERSIST},fetch = FetchType.EAGER)
    @JoinColumn(name = "userId")
    private User user;

    /**
     * 保养花费
     */
    @Column(name = "cost")
    private Double cost;

    @Column(name = "createTime")
    private Date createTime;

    @Column(name = "plate")
    private String plate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public MaintainType getMaintainType() {
        return maintainType;
    }

    public void setMaintainType(MaintainType maintainType) {
        this.maintainType = maintainType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Maintain maintain = (Maintain) o;

        if (id != maintain.id) return false;
        if (info != null ? !info.equals(maintain.info) : maintain.info != null) return false;
        if (maintainType != null ? !maintainType.equals(maintain.maintainType) : maintain.maintainType != null)
            return false;
        if (user != null ? !user.equals(maintain.user) : maintain.user != null) return false;
        if (cost != null ? !cost.equals(maintain.cost) : maintain.cost != null) return false;
        return createTime != null ? createTime.equals(maintain.createTime) : maintain.createTime == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (info != null ? info.hashCode() : 0);
        result = 31 * result + (maintainType != null ? maintainType.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (cost != null ? cost.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        return result;
    }
}
