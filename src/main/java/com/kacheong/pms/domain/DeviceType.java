package com.kacheong.pms.domain;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by kacheong on 2017/11/16.
 */
@Entity
@Table(name = "deviceType")
public class DeviceType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * 设备类型 e.g. 消防栓
     */
    @Column(name = "type")
    private String type;

    @OneToMany(mappedBy = "deviceType",cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    private Set<Device> deviceSet;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<Device> getDeviceSet() {
        return deviceSet;
    }

    public void setDeviceSet(Set<Device> deviceSet) {
        this.deviceSet = deviceSet;
    }
}
