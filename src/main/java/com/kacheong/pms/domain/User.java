package com.kacheong.pms.domain;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Created by kacheong on 2017/11/15.
 */
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * 员工/客户名字
     */
    @Column(name = "name")
    private String name;

    @Column(name ="password")
    private String password;
    /**
     * 用户的电话
     */
    @Column(name = "tel")
    private String tel;

    /**
     * 用户的角色ID
     */
    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE},fetch = FetchType.EAGER)
    @JoinColumn(name = "roleId")
    private Role role;

    @OneToMany(mappedBy = "user",cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    private Set<Advertisement> advertisementSet;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Set<Advertisement> getAdvertisementSet() {
        return advertisementSet;
    }

    public void setAdvertisementSet(Set<Advertisement> advertisementSet) {
        this.advertisementSet = advertisementSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (!name.equals(user.name)) return false;
        if (!password.equals(user.password)) return false;
        return tel.equals(user.tel);
    }

}
