package com.kacheong.pms.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "vipRecord")
public class VipRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * 注册会员时间
     */
    @Column(name = "createTime")
    private Date createTime;

    /**
     * 选择会员时长
     */
    @Column(name = "month")
    private Integer month;

    /**
     * 花费
     */
    @Column(name = "cost")
    private Double cost;
    /**
     * 车辆的ID
     */
    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.PERSIST},fetch = FetchType.EAGER)
    @JoinColumn(name = "carId")
    private Car car;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
}
