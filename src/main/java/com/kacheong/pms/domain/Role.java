package com.kacheong.pms.domain;

import javax.persistence.*;

import java.util.Set;

/**
 * Created by kacheong on 2017/11/15.
 */
@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * 角色
     */
    @Column(name = "role")
    private String role;

    @OneToMany(mappedBy = "role",cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    private Set<User> userSet;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Set<User> getUserSet() {
        return userSet;
    }

    public void setUserSet(Set<User> userSet) {
        this.userSet = userSet;
    }
}
