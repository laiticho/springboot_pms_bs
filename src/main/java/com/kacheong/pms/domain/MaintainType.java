package com.kacheong.pms.domain;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Created by kacheong on 2017/11/16.
 */
@Entity
@Table(name = "maintainType")
public class MaintainType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * 保养类型 e.g. 洗车
     */
    @Column(name = "type")
    private String type;

    @OneToMany(mappedBy = "maintainType",cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    private Set<Maintain> maintainSet;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<Maintain> getMaintainSet() {
        return maintainSet;
    }

    public void setMaintainSet(Set<Maintain> maintainSet) {
        this.maintainSet = maintainSet;
    }
}
