package com.kacheong.pms.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by kacheong on 2017/11/14.
 */
@Entity
@Table(name = "car")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * 车牌号
     */
    @Column(name = "plate",nullable = false,unique = true)
    private String plate;

    @Column(name = "day")
    private Long day;

    @OneToMany(mappedBy = "car",cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    private Set<CarPosition> carPositionSet;

    @OneToMany(mappedBy = "car",cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    private Set<VipRecord> vipRecordSet;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public Long getDay() {
        return day;
    }

    public void setDay(Long day) {
        this.day = day;
    }

    public Set<CarPosition> getCarPositionSet() {
        return carPositionSet;
    }

    public void setCarPositionSet(Set<CarPosition> carPositionSet) {
        this.carPositionSet = carPositionSet;
    }
}
