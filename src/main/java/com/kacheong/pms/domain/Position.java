package com.kacheong.pms.domain;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Created by kacheong on 2017/11/14.
 */
@Entity
@Table(name = "position")
public class Position {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * 车位信息 e.g. 21号位
     */
    @Column(name = "name")
    private String name;

    @Column(name = "status")
    private String status;

    /**
     * 区域ID
     */
    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.PERSIST},fetch = FetchType.EAGER)
    @JoinColumn(name = "areaId")
    private Area area;

    @OneToMany(mappedBy = "position")
    private Set<CarPosition> carPositionSet;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Set<CarPosition> getCarPositionSet() {
        return carPositionSet;
    }

    public void setCarPositionSet(Set<CarPosition> carPositionSet) {
        this.carPositionSet = carPositionSet;
    }
}
