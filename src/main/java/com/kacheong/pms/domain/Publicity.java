package com.kacheong.pms.domain;

import javax.persistence.*;
import java.util.List;

/**
 * Created by cangya001 on 2017/11/15.
 */
@Entity
@Table(name = "publicity")
public class Publicity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * 停车场公告信息
     */
    @Column(name = "info",unique = true)
    private String info;

    /**
     * 车库ID
     */
    @ManyToOne(fetch = FetchType.EAGER,cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    @JoinColumn(name = "garageId")
    private Garage garage;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Garage getGarage() {
        return garage;
    }

    public void setGarage(Garage garage) {
        this.garage = garage;
    }
}
