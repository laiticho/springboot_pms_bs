package com.kacheong.pms.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.kacheong.pms.POJO.PageBean;
import com.kacheong.pms.dao.IRoleDao;
import com.kacheong.pms.dao.IUserDao;
import com.kacheong.pms.domain.Role;
import com.kacheong.pms.domain.User;
import com.kacheong.pms.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/person")
public class PersonController {
    @Autowired
    private IUserDao userDao;

    @Autowired
    private IPersonService personService;

    @Autowired
    private IRoleDao roleDao;

    @RequestMapping("/editUserRole")
    public JSONObject editUserRole(@RequestParam Integer userId, Integer roleId){
        JSONObject jsonObject = new JSONObject();
        String result = personService.editUserRole(userId, roleId);
        jsonObject.put("msg", result);
        return jsonObject;
    }

    @RequestMapping("/list")
    public JSONArray getList(){
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        List list =  roleDao.list();
        for (Object aList : list) {
            Role role = (Role) aList;
            jsonObject.put("role", role.getRole());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    @RequestMapping("/getByUser")
    public JSONObject getByUser(@RequestParam Integer id){
        JSONObject jsonObject = new JSONObject();
        Role role = personService.getRoleByUser(id);
        jsonObject.put("roleId", role.getId());
        jsonObject.put("roleName",role.getRole());
        return jsonObject;
    }

    @RequestMapping(value = "/getByName")
    public JSONObject getByName(@RequestBody Map<String,Object> params){
        JSONObject jsonObject = new JSONObject();
        String name = (String) params.get("username");
        User user = userDao.getByName(name);
        jsonObject.put("username",user.getName());
        jsonObject.put("role",user.getRole().getRole());
        jsonObject.put("tel",user.getTel());
        return jsonObject;
    }

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public JSONObject login(@RequestBody Map<String,Object> params){
        String name = (String) params.get("username");
        String password = (String) params.get("password");
        String result = personService.login(name,password);
        if (result!=null){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("result",result);
            return jsonObject;
        }
        return null;
    }

    /**
     * 获得角色为客户的用户
     * @param params
     * @return 封装客户信息的Json数组
     */
    @RequestMapping(value = "/getCustomer")
    public JSONArray getCustomer(@RequestBody Map<String,Object> params){
        JSONArray jsonArray = new JSONArray();
        Integer currentPage = (int) params.get("currentPage");
        String page = (String) params.get("page");
        String hql = "FROM User user WHERE user.role.id = 2";
        PageBean pageBean = userDao.doPage(hql,currentPage,10);
        List list = pageBean.getList();
        for (Object aList : list) {
            User user = (User) aList;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", user.getId());
            jsonObject.put("name", user.getName());
            jsonObject.put("tel", user.getTel());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }
}
