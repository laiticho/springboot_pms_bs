package com.kacheong.pms.Util;

import easypr.core.PlateDetect;

import java.util.Calendar;
import java.util.Date;

public class NumberUtil {

    /**
     * 秒换时，>1 --- =2
     * @param second
     * @return hour
     */
    public Integer secondToHour(Integer second){
        Integer hour;
        if (second!=0){
            if (second%3600!=0){
                hour = second/3600+1;
                return hour;
            }
            else {
                hour = second/3600;
                return hour;
            }
        }
        return 0;
    }

    /**
     * 计算当前日期购买N月卡  vip会剩余多少天
     * @param month
     * @return
     */
    public Long monthToDay(Integer month){
        Long day;
        Calendar calendar = Calendar.getInstance();
        Date now = new Date();
        calendar.set(Calendar.MONTH,calendar.get(Calendar.MONTH)+month);
        Date date= calendar.getTime();
        day = (date.getTime()-now.getTime())/(24*60*60*1000);
        PlateDetect plateDetect = new PlateDetect();
        return day;
    }

}
