package com.kacheong.pms.Value;


import java.util.Random;

public class StaticValue {
    public static String success = "success";
    public static String error = "error";
    public static double discount = 0.8;
    //自动生成粤******车牌
    public static String AutoPlate(){
        StringBuilder plate = new StringBuilder("粤");
        String chr = "ABCDEFGHJKLMNPQRSTUVWXYZ";
        plate.append(chr.charAt((int) (Math.random()*24)));
        plate.append(chr.charAt((int) (Math.random()*24)));
        for (int i=0;i<4;i++){
            Random random = new Random();
            int j = random.nextInt(10);
            plate.append(j);
        }
        return String.valueOf(plate);
    }

}
