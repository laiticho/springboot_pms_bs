package com.kacheong.pms.POJO;

import java.util.List;

/**
 * Created by kacheong on 2017/11/17.
 */
@SuppressWarnings("unchecked")
public class PageBean {
    /**
     * 初始化分页信息
     */
    public PageBean( long allRow, int pageSize, int currentPage) {
        this.allRow = allRow;
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.totalPage = countTotalPage();
    }

    private List list;// 要返回的某一页的记录列表
    private long allRow; // 总记录数
    private int totalPage; // 总页数
    private int currentPage; // 当前页
    private int pageSize;// 每页记录数

    public PageBean() {

    }


    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public long getAllRow() {
        return allRow;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    /**
     * 计算总页数,静态方法,供外部直接通过类名调用
     *
     * @return 总页数
     */

    private int countTotalPage() {
        totalPage = (int) (allRow % pageSize == 0 ? allRow / pageSize : allRow / pageSize + 1);
        return totalPage;
    }
}
