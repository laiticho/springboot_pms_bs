package easypr.test;

import org.bytedeco.javacpp.opencv_core.Mat;
import easypr.core.CharsRecognise;
import easypr.core.PlateDetect;
import org.junit.Test;

import java.io.File;
import java.util.Vector;

import static org.bytedeco.javacpp.opencv_highgui.imread;

public class GeneralTest {
	  
    @Test
    public void testPlateRecognise() {
    	System.out.println(System.getProperty("java.library.path"));
 
    
    	//File generalPath=new File("res/image/test_image");
    	File generalPath=new File("B:\\IDEAProject\\pms\\src\\main\\res\\image\\general_test2");
        //String imgPath = "res/image/test_image/test.jpg";
        // String imgPath = "res/image/test_image/plate_recognize.jpg";
    	File[] files=generalPath.listFiles(); 
    	 System.out.println("files："+files.length);
    	for (int j = 0; j< files.length; j++) {
    		 System.out.println("j："+j);
    		try{
    			File file = files[j];
			    System.out.println("当前文件名称："+file.getPath());
				Mat src = imread(file.getPath());
		        PlateDetect plateDetect = new PlateDetect();
		        plateDetect.setPDLifemode(true);
		        Vector<Mat> matVector = new Vector<Mat>();
		        
		        if (0 == plateDetect.plateDetect(src, matVector)) {
		            CharsRecognise cr = new CharsRecognise();

		            for (int i = 0; i < matVector.size(); ++i) {
		                String result = cr.charsRecognise(matVector.get(i));
		                System.out.println("Chars Recognised: " + result);
		                break;
		            }
		        }
    		}catch (Throwable e){
    			System.out.println(e.getMessage());
    		}
		}
      
    }

//    @Test
//	public void testIn(){


//	}
}
